package com.mathfriendzy.model.singleFriendzy;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.model.learningcenter.PlayerDataFromServerObj;

public class SingleFriendzyServerOperation 
{
	/**
	 * This method get the single friendzy detail from server
	 * @param userId
	 * @param playerId
	 */
	public PlayerDataFromServerObj getSingleFriendzyDetail(String userId, String playerId)
	{
		String action = "getSingleFriendzyDetail";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "user_id="   + userId  + "&"
				+ "player_id=" + playerId + "&appId="+CommonUtils.APP_ID;
		
		//Log.e("SingleFriendzyServerOperation", strUrl);
		
		return this.parseJsonForSingleFriendzyDetail(CommonUtils.readFromURL(strUrl));
	}
	
	
	/**
	 * This method get the points level detail from server
	 * @param userId
	 * @param playerId
	 */
	public PlayerDataFromServerObj getPointsLevelDetail(String userId , String playerId)
	{
		String action = "getSingleFriendzyDetail";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "user_id="   + userId  + "&"
				+ "player_id=" + playerId + "&appId="+CommonUtils.APP_ID;
		
		//Log.e("SingleFriendzyServerOperation", strUrl);
		
		return this.parseJsonForpointLevelDetail(CommonUtils.readFromURL(strUrl));
	}
	
	
	/**
	 * This method parse json for single friendzy detail
	 * @param jsonString
	 * @return
	 */
	private PlayerDataFromServerObj parseJsonForSingleFriendzyDetail(String jsonString)
	{
		//Log.e("SingleFriendzyServerOperation", jsonString);
		
		PlayerDataFromServerObj playerData = new PlayerDataFromServerObj();
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONObject jsonObject2 = jObject.getJSONObject("data");
			try{
				playerData.setPoints(jsonObject2.getInt("point"));
			}catch(Exception e){}
			playerData.setItems(jsonObject2.getString("items"));
			playerData.setCompleteLevel(jsonObject2.getInt("level"));
			playerData.setLockStatus(jsonObject2.getInt("appsUnlock"));
			
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		return playerData;
	}
	
	
	/**
	 * This method parse json for point level detail for single friendzy
	 * @param jsonString
	 * @return
	 */
	private PlayerDataFromServerObj parseJsonForpointLevelDetail(String jsonString)
	{
		//Log.e("SingleFriendzyServerOperation", jsonString);
		
		PlayerDataFromServerObj playerData = new PlayerDataFromServerObj();
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONObject jsonObject2 = jObject.getJSONObject("data");
			try{
				playerData.setPoints(jsonObject2.getInt("point"));
			}catch(Exception e){}
			playerData.setCompleteLevel(jsonObject2.getInt("level"));
			playerData.setLockStatus(jsonObject2.getInt("appsUnlock"));
			
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		return playerData;
	}
	
	/**
	 * This method find challenger from server for temp player
	 * @param grade
	 * @param completeLevel
	 */
	public ChallengerDataFromServerTranseferObj findChallengerForTempPlayer(int grade , int completeLevel)
	{
		String action = "findChallengerForTemporaryPlayer";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "grade="   + grade  + "&"
				+ "competeLevel=" + completeLevel+ "&appId="+CommonUtils.APP_ID;
		
		//Log.e("SingleFriendzyServerOperation", "inside findChallengerForTempPlayer url : " + strUrl);
		return this.parseJsonForChallenger(CommonUtils.readFromURL(strUrl));
	}
	
	/**
	 * this method find challenger from server for login user player
	 * @param userId
	 * @param playerId
	 */
	public ChallengerDataFromServerTranseferObj findChallangerForLoginPlayer(String userId , String playerId)
	{
		String action = "findChallenger";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId+ "&appId="+CommonUtils.APP_ID;
		
		//Log.e("SingleFriendzyServerOperation", "inside findChallangerForLoginPlayer url : " + strUrl);
		return this.parseJsonForChallenger(CommonUtils.readFromURL(strUrl));
	}
	
	/**
	 * This method parse json string for find challenger
	 * @param jsonString
	 */
	private ChallengerDataFromServerTranseferObj parseJsonForChallenger(String jsonString)
	{
		//Log.e("SingleFriendzyServerOperation", "inside parse challenger json : " + jsonString);
		
		ChallengerDataFromServerTranseferObj challengerData = new ChallengerDataFromServerTranseferObj();
		ArrayList<ChallengerTransferObj> challenegreList = new ArrayList<ChallengerTransferObj>();
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONObject jsonObject2 = jObject.getJSONObject("data");
			JSONArray jsonChallengerArray = jsonObject2.getJSONArray("challengers");
			
			for( int i = 0 ; i < jsonChallengerArray.length() ; i ++ )
			{
				JSONObject jsonObj = jsonChallengerArray.getJSONObject(i);
				
				ChallengerTransferObj challengerObj = new ChallengerTransferObj();
				
				challengerObj.setFirst(jsonObj.getString("first"));
				challengerObj.setLast(jsonObj.getString("last"));
				challengerObj.setPlayerId(jsonObj.getString("playerId"));
				challengerObj.setUserId(jsonObj.getString("userId"));
				challengerObj.setSum(jsonObj.getString("sum"));
				challengerObj.setCountryIso(jsonObj.getString("country"));
				challengerObj.setIsFakePlayer(jsonObj.getInt("isFakePlayer"));
				
				challenegreList.add(challengerObj);
			}
			
			challengerData.setPushNotificationDevice(jsonObject2.getString("pushNotifDevices"));
			challengerData.setChallengerPlayerList(challenegreList);
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		return challengerData;
	}
	
	/**
	 * This method get the highest score from the server
	 * @param userId
	 * @param playerId
	 */
	public int getHighestScore(String userId, String playerId)
	{
		String action = "getHighestScore";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId+ "&appId="+CommonUtils.APP_ID;
		
		//Log.e("SingleFriendzyServerOperation", "inside getHighestScore url : " + strUrl);
		return this.parsegetHighestScoreJson(CommonUtils.readFromURL(strUrl));
	}
	
	/**
	 * This method parse json for the highest score
	 * @param jsonString
	 */
	private int parsegetHighestScoreJson(String jsonString)
	{
		//Log.e("SingleFriendzyServerOperation", "inside parse parsegetHighestScoreJson json : " + jsonString);
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONObject jsonObject2 = jObject.getJSONObject("data");
			
			return jsonObject2.getInt("highestScore");
		}
		catch (JSONException e) 
		{
			Log.e("SingleFriendzyServerOperation", "No Highest Score exist :" + e.toString());
			return 0;
		}
	}
	
	/**
	 * This method get equations from server for single friendzy
	 * @param userId
	 * @param playerId
	 */
	public ArrayList<EquationFromServerTransferObj> getEquations(String userId , String playerId)
	{
		String action = "findCompeteEquatiosForPlayer";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "playerId="   + playerId  + "&"
				+ "userId=" + userId+ "&appId="+CommonUtils.APP_ID;
		
		//Log.e("SingleFriendzyServerOperation", "inside getEquations url : " + strUrl);
		return this.parseJsonForGetEquations(CommonUtils.readFromURL(strUrl));
	}
	
	/**
	 * This method parse json for equation from server
	 * @param jsonString
	 * @return
	 */
	private ArrayList<EquationFromServerTransferObj> parseJsonForGetEquations(String jsonString)
	{
		ArrayList<EquationFromServerTransferObj> equationList = new ArrayList<EquationFromServerTransferObj>();
		
		//Log.e("SingleFriendzyServerOperation", "inside parse parseJsonForGetEquations json : " + jsonString);
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONArray jsonObject2 = jObject.getJSONArray("data");
			
			for(int i = 0 ; i < jsonObject2.length() ; i ++ )
			{
				JSONObject jsonObj = jsonObject2.getJSONObject(i);
				
				EquationFromServerTransferObj equationObj = new EquationFromServerTransferObj();
				equationObj.setUserId(jsonObj.getString("userId"));
				equationObj.setPlayerId(jsonObj.getString("playerId"));
				//equationObj.setIsFakePlayer(jsonObj.getInt("isFakePlayer"));
				equationObj.setMathEquationId(jsonObj.getInt("questionId"));
				//equationObj.setLap(jsonObj.getInt("lap"));
				equationObj.setPoints(jsonObj.getInt("points"));
				equationObj.setIsAnswerCorrect(jsonObj.getInt("isAnswerCorrect"));
				equationObj.setTimeTakenToAnswer(jsonObj.getDouble("timeTakenToAnswer"));
				
				equationList.add(equationObj);
			}
			
			return equationList;
		}
		catch (JSONException e) 
		{
			Log.e("SingleFriendzyServerOperation", "No Equation exist Score exist :" + e.toString());
			return null;
		}
	}
	
	/**
	 * This method add math complete level for bonus
	 * @param userId
	 * @param playerId
	 * @param level
	 * @param stars
	 * @param appId
	 */
	public void addMathComplteLevelForBonus(String userId , String playerId , int level , int stars , String appId)
	{
		String action = "addFriendzyCompeteLevelForBonus";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId+ "&"
				+ "level=" + level + "&"
				+ "star=" + stars + "&"
				+ "appId=" + appId;
		
		//Log.e("SingleFriendzyServerOperation", "inside addMathComplteLevelForBonus url : " + strUrl);
		this.parseJsonForAddMathCompleteLevelForBonus(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse json form ammMathCompleteLevelforBonus
	 */
	private void parseJsonForAddMathCompleteLevelForBonus(String jsonString)
	{
		//Log.e("SingleFriendzyServerOperation", "inside addMathComplteLevelForBonus url : " + jsonString);
	}
}
