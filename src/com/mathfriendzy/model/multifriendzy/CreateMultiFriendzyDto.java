package com.mathfriendzy.model.multifriendzy;

public class CreateMultiFriendzyDto 
{
	private String problems;
	private String friendzyId;
	private String playerUserId;
	private String playerId;
	private String opponentUserId;
	private String opponentPlayerId;
	private int roundScore;
	private int points;
	private int coins;
	private String date;
	private int modifying;
	private int isCompleted;
	private String winner;
	private int shouldSaveEquations;
	
	public String getProblems() {
		return problems;
	}
	public void setProblems(String problems) {
		this.problems = problems;
	}
	public String getFriendzyId() {
		return friendzyId;
	}
	public void setFriendzyId(String friendzyId) {
		this.friendzyId = friendzyId;
	}
	public String getPlayerUserId() {
		return playerUserId;
	}
	public void setPlayerUserId(String playerUserId) {
		this.playerUserId = playerUserId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getOpponentUserId() {
		return opponentUserId;
	}
	public void setOpponentUserId(String opponentUserId) {
		this.opponentUserId = opponentUserId;
	}
	public String getOpponentPlayerId() {
		return opponentPlayerId;
	}
	public void setOpponentPlayerId(String opponentPlayerId) {
		this.opponentPlayerId = opponentPlayerId;
	}
	public int getRoundScore() {
		return roundScore;
	}
	public void setRoundScore(int roundScore) {
		this.roundScore = roundScore;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getModifying() {
		return modifying;
	}
	public void setModifying(int modifying) {
		this.modifying = modifying;
	}
	public int getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(int isCompleted) {
		this.isCompleted = isCompleted;
	}
	public String getWinner() {
		return winner;
	}
	public void setWinner(String winner) {
		this.winner = winner;
	}
	public int getShouldSaveEquations() {
		return shouldSaveEquations;
	}
	public void setShouldSaveEquations(int shouldSaveEquations) {
		this.shouldSaveEquations = shouldSaveEquations;
	}
	
}
