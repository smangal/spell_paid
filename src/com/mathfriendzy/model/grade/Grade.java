package com.mathfriendzy.model.grade;

import java.util.ArrayList;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mathfriendzy.database.Database;

import static com.mathfriendzy.utils.ICommonUtils.GRADE_FLAG;

public class Grade 
{
	//grade label table
	private String GRADE_TABLE  			= "grade_levels";
	private String GRADE_LEVEL				= "grade_level";
	private final String TAG				= this.getClass().getSimpleName();
	
	/**
	 * Return grade List from database
	 * @param context
	 * @return
	 */
	public ArrayList<String> getGradeList(Context context)
	{
		if(GRADE_FLAG)
			Log.e(TAG, "inside getGradeList()");
		
		ArrayList<String> gradeList = new ArrayList<String>();
		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		String query = "select " + GRADE_LEVEL + " from " + GRADE_TABLE;
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext())
		{		
			gradeList.add(cursor.getString(cursor.getColumnIndex(GRADE_LEVEL)));
		}
		cursor.close();
		dbConn.close();

		if(GRADE_FLAG)
			Log.e(TAG, "outside getGradeList()");
		
		return gradeList;
	}
}
