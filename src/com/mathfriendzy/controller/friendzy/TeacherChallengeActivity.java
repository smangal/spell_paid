package com.mathfriendzy.controller.friendzy;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.teacherStudents.TeacherStudents;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.spellfriendzypaid.R;

public class TeacherChallengeActivity extends AdBaseActivity implements OnClickListener 
{
	private TextView mfTitleHomeScreen			= null;
	//private TextView txtPlayTitle				= null;
	//private TextView lblTeacher					= null;
	//private TextView lblStudent					= null;

	private ImageButton btnInfoStudent			= null;
	private ImageButton btnInfoFriendzy			= null;
	//private ImageButton ImgBtnTeacher			= null;
	//private ImageButton btnInfoPlayer			= null;
//	private ImageButton ImgBtnPlayer			= null;
	
	//private RelativeLayout	layoutStudent		= null;
	//private RelativeLayout	layoutPlayers		= null;

	private Button btnMyStudents				= null;
	private Button btnFriendzyChallenge			= null;
	//private Button btnMyPlayers					= null;

	private String studentInfo					= "";
	//private String playerInfo					= "";
	private String challengerInfo				= "";

	//private boolean isTeacher					= true;
	//private boolean isStudent					= false;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_teacher_challenge);

		getWidgetId();
		setTextOnWidget();

		setListener();


	}//END onCreate method



	private void setTextOnWidget()
	{
		Translation translate = new Translation(this);
		translate.openConnection();
		//lblFunctions pickerTitleTeacher
		mfTitleHomeScreen.setText(translate.getTranselationTextByTextIdentifier("pickerTitleTeacher")+" "+
				translate.getTranselationTextByTextIdentifier("lblFunctions"));
		//txtPlayTitle.setText(translate.getTranselationTextByTextIdentifier("lblAreYouPlayingAs")+":");
		//lblTeacher.setText(""+translate.getTranselationTextByTextIdentifier("pickerTitleTeacher"));
		//lblStudent.setText(translate.getTranselationTextByTextIdentifier("lblStudent"));

		btnMyStudents.setText(translate.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
		btnFriendzyChallenge.setText(translate.getTranselationTextByTextIdentifier("btnTitleChallenges"));
		//btnMyPlayers.setText(translate.getTranselationTextByTextIdentifier("btnTitleMyPlayers"));

		studentInfo		= translate.getTranselationTextByTextIdentifier("alertMsgMyStudentsInfo");
		challengerInfo	= translate.getTranselationTextByTextIdentifier("alertMsgFriendzyChallenesInfo");
		//playerInfo		= translate.getTranselationTextByTextIdentifier("alertMsgMyPlayersInfo");

		/*infoMsg = translate.getTranselationTextByTextIdentifier("lblFriendzyChallengeWeek")+" \n\n "+
				translate.getTranselationTextByTextIdentifier("lblAskYourTeacherToDownload")+" "+
				translate.getTranselationTextByTextIdentifier(ITextIds.MF_HOMESCREEN)+" "+
				translate.getTranselationTextByTextIdentifier("lblAndCreateAnAccountAsTeacher")+" \n\n "+
				translate.getTranselationTextByTextIdentifier("lblYourTeacherWillBeableToChooseDurationOfChallenge");*/

		translate.closeConnection();

	}//END setTextonWidget method


	private void getWidgetId()
	{
		mfTitleHomeScreen 	 = (TextView) findViewById(R.id.mfTitleHomeScreen);
		//txtPlayTitle		 = (TextView) findViewById(R.id.txtPlayTitle);
		//lblTeacher			 = (TextView) findViewById(R.id.lblTeacher);
		//lblStudent			 = (TextView) findViewById(R.id.lblStudent);

		btnMyStudents		 = (Button) findViewById(R.id.btnMyStudents);
		//btnMyPlayers		 = (Button) findViewById(R.id.btnMyPlayers);		
		btnFriendzyChallenge = (Button) findViewById(R.id.btnFriendzyChallenge);

		btnInfoStudent			= (ImageButton) findViewById(R.id.btnInfoStudent);
		btnInfoFriendzy			= (ImageButton) findViewById(R.id.btnInfoFriendzy);
		//btnInfoPlayer 			= (ImageButton) findViewById(R.id.btnInfoPlayers);	

		//ImgBtnPlayer			= (ImageButton) findViewById(R.id.ImgBtnStudent);
		//ImgBtnTeacher			= (ImageButton) findViewById(R.id.ImgBtnTeacher);
		
		//layoutPlayers			= (RelativeLayout) findViewById(R.id.layoutPlayers);
		//layoutStudent			= (RelativeLayout) findViewById(R.id.layoutStudents);
		
	}//END getWidgetId method


	private void setListener()
	{		
		btnInfoStudent.setOnClickListener(this);
		btnInfoFriendzy.setOnClickListener(this);
		//btnInfoPlayer.setOnClickListener(this);
		//ImgBtnPlayer.setOnClickListener(this);
		//ImgBtnTeacher.setOnClickListener(this);

		btnMyStudents.setOnClickListener(this);
		//btnMyPlayers.setOnClickListener(this);
		btnFriendzyChallenge.setOnClickListener(this);

	}


	@Override
	public void onClick(View v)
	{		
		DialogGenerator dg = null;
		switch(v.getId())
		{
		case R.id.btnInfoStudent:
			dg = new DialogGenerator(this);
			dg.generateFriendzyWarningDialog(studentInfo);
			break;

		/*case R.id.btnInfoPlayers:
			dg = new DialogGenerator(this);
			dg.generateFriendzyWarningDialog(playerInfo);
			break;*/

		case R.id.btnInfoFriendzy:
			dg = new DialogGenerator(this);
			dg.generateFriendzyWarningDialog(challengerInfo);
			break;	

		case R.id.btnMyStudents:
			Intent intentStudents = new Intent(this,TeacherStudents.class);
			intentStudents.putExtra("callingActivity", "TeacherChallengeActivity");
			startActivity(intentStudents);
			break;

		/*case R.id.btnMyPlayers:
			Intent intent = new Intent(this,TeacherPlayer.class);
			intent.putExtra("calling", "TeacherChallengeActivity");
			startActivity(intent);
			break;*/

		case R.id.btnFriendzyChallenge:
			checkTeacherOrPlayer();
			
			break;

		/*case R.id.ImgBtnStudent:
			if(MainActivity.isTab)
			{
				ImgBtnPlayer.setBackgroundResource(R.drawable.tab_checkbox_checked_ipad);			
				ImgBtnTeacher.setBackgroundResource(R.drawable.tab_checkbox_unchecked_ipad);
			}
			else
			{
				ImgBtnPlayer.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);			
				ImgBtnTeacher.setBackgroundResource(R.drawable.mf_check_box_ipad);
			}
			setVisibility(View.INVISIBLE);

			isTeacher = false;
			isStudent = true;			
			break;*/
			
		/*case R.id.ImgBtnTeacher:
			if(MainActivity.isTab)
			{
				ImgBtnPlayer.setBackgroundResource(R.drawable.tab_checkbox_unchecked_ipad);			
				ImgBtnTeacher.setBackgroundResource(R.drawable.tab_checkbox_checked_ipad);
			}
			else
			{
				ImgBtnTeacher.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);			
				ImgBtnPlayer.setBackgroundResource(R.drawable.mf_check_box_ipad);
			}
			setVisibility(View.VISIBLE);
			isTeacher = true;
			isStudent = false;			
			break;*/

		}//End switch case

	}//END onClick method
	
	/*private void setVisibility(int view)
	{
		//layoutPlayers.setVisibility(view);
		layoutStudent.setVisibility(view);
		btnInfoFriendzy.setVisibility(view);
		
	}//END setVisibility method
*/	


	private void checkTeacherOrPlayer()
	{
		UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		RegistereUserDto regUserObj = userObj.getUserData();
		String userId = regUserObj.getUserId();
		
		SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 
		Editor editor = sheredPreferencePlayer.edit();
		editor.putString("userId", userId);
		editor.commit();
		Intent intent = new Intent(this,StudentChallengeActivity.class);
		intent.putExtra("isTeacher", true);
		startActivity(intent);
		/*if(isStudent)
		{		
			
			if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
			{				
				if(regUserObj.getIsParent().equals("0"))//0 for teacher
				{
					Intent intent = new Intent(this,TeacherPlayer.class);
					intent.putExtra("calling", "TeacherChallengeActivity");
					this.startActivity(intent);
				}
				else
				{
					Intent intent = new Intent(this,LoginUserPlayerActivity.class);
					intent.putExtra("calling", "TeacherChallengeActivity");
					this.startActivity(intent);
				}
			}
			else
			{
				Intent intent = new Intent(this,StudentChallengeActivity.class);
				startActivity(intent);
			}
		}
		else
		{
			Intent intent = new Intent(this,StudentChallengeActivity.class);
			intent.putExtra("isTeacher", isTeacher);
			startActivity(intent);
		}*/
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{	
			startActivity(new Intent(this, MainActivity.class));
			finish();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

}
