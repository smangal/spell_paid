package com.mathfriendzy.controller.friendzy.calender;

import java.util.Calendar;
import java.util.Locale;

import android.annotation.SuppressLint;
import com.mathfriendzy.controller.base.AdBaseActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.spellfriendzypaid.R;

public class CalenderView extends AdBaseActivity implements OnClickListener
{

	private static final String tag = "SimpleCalendarViewActivity";

	private TextView currentMonth;
	private ImageView prevMonth;
	private ImageView nextMonth;
	private GridView calendarView;
	private GridCellAdapter adapter;
	private Calendar _calendar;
	//private ListView newList;
	private int month, year;

	@SuppressLint("NewApi")
	private static final String dateTemplate = "MMMMMMMMM yyyy";


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calender_view);

		_calendar = Calendar.getInstance(Locale.getDefault());
		month = _calendar.get(Calendar.MONTH) + 1;
		year = _calendar.get(Calendar.YEAR);

		prevMonth = (ImageView) this.findViewById(R.id.prevMonth);
		prevMonth.setOnClickListener(this);

		currentMonth = (TextView) this.findViewById(R.id.currentMonth);
		currentMonth.setText(DateFormat.format(dateTemplate, _calendar.getTime()));

		nextMonth = (ImageView) this.findViewById(R.id.nextMonth);
		nextMonth.setOnClickListener(this);

		calendarView = (GridView) this.findViewById(R.id.calendar);
		calendarView.setEnabled(false);

		// Initialised
		adapter = new GridCellAdapter(this, month, year, calendarView, -1);
		adapter.notifyDataSetChanged();
		calendarView.setAdapter(adapter);

	}



	/**
	 * 
	 * @param month
	 * @param year
	 */
	private void setGridCellAdapterToDate(int month, int year)
	{
		adapter = new GridCellAdapter(this, month, year, calendarView, -1);
		_calendar.set(year, month - 1, _calendar.get(Calendar.DAY_OF_MONTH));
		currentMonth.setText(DateFormat.format(dateTemplate, _calendar.getTime()));
		adapter.notifyDataSetChanged();
		calendarView.setAdapter(adapter);
	}


	@Override
	public void onClick(View v) 
	{
		if (v == prevMonth)
		{
			if (month <= 1)
			{
				month = 12;
				year--;
			}
			else
			{
				month--;
			}
			
			setGridCellAdapterToDate(month, year);
		}
		if (v == nextMonth)
		{
			if (month > 11)
			{
				month = 1;
				year++;
			}
			else
			{
				month++;
			}
			
			setGridCellAdapterToDate(month, year);
		}

	}
}
