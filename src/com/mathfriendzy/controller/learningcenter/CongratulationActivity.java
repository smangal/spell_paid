package com.mathfriendzy.controller.learningcenter;

import static com.mathfriendzy.utils.ICommonUtils.BTN_COM_LINK_URL;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ITextIds.RATE_URL;
import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mathfriendzy.controller.learningcenter.addition_subtraction_of_decimal.Measurement;
import com.mathfriendzy.controller.learningcenter.addition_subtraction_of_fraction.Money;
import com.mathfriendzy.controller.learningcenter.addition_subtraction_of_negative_number.SocialStudies;
import com.mathfriendzy.controller.learningcenter.division.Time;
import com.mathfriendzy.controller.learningcenter.multiplication.Language;
import com.mathfriendzy.controller.learningcenter.multiplication_division_of_decimals.Geography;
import com.mathfriendzy.controller.learningcenter.multiplication_division_of_fraction.Phonics;
import com.mathfriendzy.controller.learningcenter.multiplication_division_of_negative_number.Science;
import com.mathfriendzy.controller.learningcenter.reading.Reading;
import com.mathfriendzy.controller.learningcenter.subtraction.Volume;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ITextIds;
import com.spellfriendzypaid.R;
import com.spellfriendzy.facebookconnect.ShareActivity;

public class CongratulationActivity extends AdBaseActivity implements OnClickListener
{
	private TextView txtTileScreen				= null;
	private TextView lblTheGameOfLearning		= null;
	private TextView txtGoodEffort				= null;
	private TextView txtEarned					= null;
	private TextView txtEarnedPoints			= null;
	private TextView txtPlayAgain				= null;
	private Button	 btnAnswer					= null;
	private Button	 btnPlay					= null;
	private Button	 btnCom						= null;
	private Button	 btnShare					= null;
	private String   lblPts						= null;

	private Button btnCloseShareTool		= null;
	private LinearLayout layoutShare		= null;
	private Button btnScreenShot			= null;
	private Button btnEmail					= null;
	private Button btnTwitter				= null;
	private Button btnFbShare				= null;

	private String subject					= null;
	private String body						= null;
	private String screenText				= null;

	private String points					= "0";
	private int point						= 0;


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_congratulation);		

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		if(metrics.heightPixels >= CommonUtils.TAB_HEIGHT && metrics.widthPixels <= CommonUtils.TAB_WIDTH
				&& metrics.densityDpi <= CommonUtils.TAB_DENISITY)	{

			setContentView(R.layout.activity_congratulation_tab_low_denisity);
		}
		point			= getIntent().getIntExtra("points", 0);		
		points = CommonUtils.setNumberString(String.valueOf(point));

		this.getWidgetId();
		this.setWidgetText();
		this.getIntentValue();

		//lblGoodEffort, lblYouEraned, lblPts, lblPlayAgainToMoveToNextLevel, playTitle
		//mfLblAnswers, btnTitleShare, btnTitleCom
	}//END onCreate Method

	private void getIntentValue() 
	{

		txtEarnedPoints.setText(points + " " + lblPts);
	}

	private void setWidgetText() 
	{
		String text;
		Translation translate = new Translation(this);
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier(ITextIds.MF_HOMESCREEN);
		txtTileScreen.setText(text);

		if(point < 1000)
		{
			text = translate.getTranselationTextByTextIdentifier("lblGoodEffort");
		}
		else
		{
			text = translate.getTranselationTextByTextIdentifier("titleGreatJob");
		}
		txtGoodEffort.setText(text+"!");

		text = translate.getTranselationTextByTextIdentifier("lblTheGameOfLearning");
		lblTheGameOfLearning.setText(text);

		text = translate.getTranselationTextByTextIdentifier("lblYouEraned");
		txtEarned.setText(text);

		lblPts = translate.getTranselationTextByTextIdentifier("lblPts");
		
		if(point < 1000)
		{	
			text = translate.getTranselationTextByTextIdentifier("lblPlayAgainToMoveToNextLevel");
		}
		else
		{
			text = translate.getTranselationTextByTextIdentifier("lblCongratulations");
			text = text + ", "+translate.getTranselationTextByTextIdentifier("lblYouCanNowPlayTheNextLevel");
		}
		txtPlayAgain.setText(text);


		text = translate.getTranselationTextByTextIdentifier("mfLblAnswers");
		btnAnswer.setText(text);

		text = translate.getTranselationTextByTextIdentifier("btnTitleCom");
		btnCom.setText(text);

		text = translate.getTranselationTextByTextIdentifier("playTitle");
		btnPlay.setText(text);

		text = translate.getTranselationTextByTextIdentifier("btnTitleShare");
		btnShare.setText(text);
		screenText 	= translate.getTranselationTextByTextIdentifier("alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
		subject		= translate.getTranselationTextByTextIdentifier("infoEmailSubject");
		body		= translate.getTranselationTextByTextIdentifier("shareFBEmailMessage");
		translate.closeConnection();

	}//END setWidgetText method


	private void getWidgetId() 
	{
		txtTileScreen		= (TextView) findViewById(R.id.txtTitleScreen);		
		txtGoodEffort		= (TextView) findViewById(R.id.txtGoodEffort);
		txtEarned			= (TextView) findViewById(R.id.txtEarned);
		txtEarnedPoints		= (TextView) findViewById(R.id.txtEarnedPoints);
		txtPlayAgain        = (TextView) findViewById(R.id.txtPlayAgain);
		lblTheGameOfLearning= (TextView) findViewById(R.id.lblTheGameOfMath);

		btnAnswer			= (Button) findViewById(R.id.btnAnswer);
		btnPlay				= (Button) findViewById(R.id.btnPlay);
		btnCom				= (Button) findViewById(R.id.btnCom);
		btnShare			= (Button) findViewById(R.id.btnShare);


		btnCloseShareTool	= (Button) findViewById(R.id.btnCloseShareToolbar);
		layoutShare			= (LinearLayout) findViewById(R.id.layoutShare);
		btnEmail			= (Button) findViewById(R.id.btnMail);
		btnFbShare			= (Button) findViewById(R.id.btnFbSahre);
		btnScreenShot		= (Button) findViewById(R.id.btnScreenShot);
		btnTwitter			= (Button) findViewById(R.id.btnTwitterShare);

		btnAnswer.setOnClickListener(this);
		btnCom.setOnClickListener(this);
		btnPlay.setOnClickListener(this);
		btnShare.setOnClickListener(this);
		btnCloseShareTool.setOnClickListener(this);
		btnEmail.setOnClickListener(this);
		btnFbShare.setOnClickListener(this);
		btnScreenShot.setOnClickListener(this);
		btnTwitter.setOnClickListener(this);
	}//END getWidgetId method



	@Override
	public void onClick(View v) 
	{
		Intent intent = null;	
		Translation transeletion = null;
		layoutShare.setVisibility(View.GONE);
		btnCloseShareTool.setVisibility(View.GONE);
		View view = getWindow().getDecorView().getRootView();
		view.setDrawingCacheEnabled(true);
		Bitmap b = view.getDrawingCache();
		Top100Activity.b = b;

		switch (v.getId())
		{
		case R.id.btnAnswer:	
			startActivity(new Intent(this,SeeAnswerActivity.class));
			break;

		case R.id.btnPlay:
			this.clickOnPlay();
			break;

		case R.id.btnShare:
			layoutShare.setVisibility(View.VISIBLE);
			btnCloseShareTool.setVisibility(View.VISIBLE);
			break;

		case R.id.btnCloseShareToolbar:
			break;

		case R.id.btnScreenShot:
			CommonUtils.saveBitmap(b, "DCIM/Camera", "screen");

			DialogGenerator generator = new DialogGenerator(this);
			transeletion = new Translation(this);
			transeletion.openConnection();
			screenText 	= transeletion.getTranselationTextByTextIdentifier("alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
			generator.generateWarningDialog(screenText);
			transeletion.closeConnection();

			break;

		case R.id.btnMail:
			//*******For Sharing ScreenShot ******************//
			String path = Images.Media.insertImage(getContentResolver(), b,"ScreenShot.jpg", null);
			Uri screenshotUri = Uri.parse(path);
			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body+" "+RATE_URL);
			emailIntent.setType("image/png");
			try 
			{
				startActivity(Intent.createChooser(emailIntent, "Send email using"));
			} catch (android.content.ActivityNotFoundException ex) {
				Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
			}

			break;

		case R.id.btnFbSahre:	

			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body+" "+RATE_URL);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("flag", false);
			intent.putExtra("id", R.id.btnFbSahre);
			startActivity(intent);
			break;

		case R.id.btnTwitterShare:			
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body);
			intent.putExtra("flag", false);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("id", R.id.btnTwitterShare);
			startActivity(intent);
			break;
		case R.id.btnCom:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BTN_COM_LINK_URL)));
			break;
		}

	}//END onClick Method

	private void clickOnPlay() 
	{
		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		int operationId = sharedPreferences.getInt("operationId", 0);

		switch(operationId)
		{
		case 1:
			startActivity(new Intent(this,Reading.class));
			break;
		case 2:
			startActivity(new Intent(this,Volume.class));
			break;
		case 3:
			startActivity(new Intent(this,Language.class));
			break;
		case 4:
			startActivity(new Intent(this,Time.class));
			break;
		case 5:
			startActivity(new Intent(this,Money.class));
			break;
		case 6:
			startActivity(new Intent(this,Phonics.class));
			break;
		case 7:
			startActivity(new Intent(this,Measurement.class));
			break;
		case 8:
			startActivity(new Intent(this,Geography.class));
			break;
		case 9:
			startActivity(new Intent(this,SocialStudies.class));
			break;
		case 10:
			startActivity(new Intent(this,Science.class));
			break;
		}

	}

	@Override
	public void onBackPressed() 
	{
		Intent intent = new Intent(this,LearningCenterMain.class);
		startActivity(intent);
		super.onBackPressed();
	}



}
