package com.mathfriendzy.controller.multifriendzy.facebookfriends;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.internal.SessionTracker;
import com.facebook.model.GraphObject;
import com.mathfriendzy.controller.multifriendzy.contacts.ContactBeans;
import com.mathfriendzy.model.language.translation.Translation;
import com.spellfriendzypaid.R;

public class FacebookFriendActivity extends AdBaseActivity implements OnClickListener 
{
	//private static final List<String> PERMISSIONS 	= Arrays.asList("publish_actions","publish_stream");	

	private PendingAction pendingAction = PendingAction.NONE;	

	private enum PendingAction 
	{
		NONE,
		POST_PHOTO,
		POST_STATUS_UPDATE
	}
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	public static Session mCurrentSession;
	public static SessionTracker mSessionTracker;
	
	private ArrayList<ContactBeans>	friendRecord	= null;
	private ListView listFbFriend					= null;
	private Button btnShare							= null;	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_facebook_friend);		

		getWidgetId();
		setWidgetText();
		
		friendRecord	= new ArrayList<ContactBeans>();	
		mCurrentSession = Session.getActiveSession();
		

		if (Session.getActiveSession() == null || Session.getActiveSession().isClosed()) 
		{			
			Session.openActiveSession(this, true, callback);
		}
		
		if (Session.getActiveSession().isOpened())
		{
			Request.executeGraphPathRequestAsync(Session.getActiveSession(), "me/friends", new Request.Callback() {

				@Override
				public void onCompleted(Response response)
				{	
					friendRecord = getFriendList(response);

					Collections.sort(friendRecord, new ListComparatorUsingName());

					EfficientAdapter adapter = new EfficientAdapter(FacebookFriendActivity.this, friendRecord);
					listFbFriend.setAdapter(adapter);
					
				}				
			});	
		}

	}//END onCreate method




	private ArrayList<ContactBeans> getFriendList(Response response)
	{		
		ArrayList<ContactBeans> friendRecord = new ArrayList<ContactBeans>();
		GraphObject go  = response.getGraphObject();
		JSONObject  jso = go.getInnerJSONObject();     
		JSONArray arr;
		try {
			arr = jso.getJSONArray( "data" );
			for ( int i = 0; i < ( arr.length() ); i++ )
			{
				JSONObject json_obj = arr.getJSONObject(i);
				ContactBeans obj = new ContactBeans();
				obj.setName(json_obj.getString( "name"));
				obj.setId(json_obj.getString( "id"));

				friendRecord.add(obj);  
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return friendRecord;
	}//END getFriendList method



	private void getWidgetId() 
	{
		listFbFriend = (ListView) findViewById(R.id.listFbFriend);
		btnShare		= (Button) findViewById(R.id.btnShare);
		btnShare.setOnClickListener(this);
		
	}//END getWIdgetId method

	
	private void setWidgetText() 
	{
		Translation translate = new Translation(this);
		String text;
		translate.openConnection();
		
		text = translate.getTranselationTextByTextIdentifier("btnTitleShare");
		btnShare.setText(text);
		
		translate.closeConnection();	
	}
	

	private class ListComparatorUsingName implements Comparator<ContactBeans>
	{
		@Override
		public int compare(ContactBeans arg0, ContactBeans arg1)
		{
			return arg0.getName().compareTo(arg1.getName());
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		
		if (Session.getActiveSession().isOpened())
		{			
			Request.executeGraphPathRequestAsync(Session.getActiveSession(), "me/friends", new Request.Callback() {

				@Override
				public void onCompleted(Response response)
				{	
					friendRecord = getFriendList(response);

					Collections.sort(friendRecord, new ListComparatorUsingName());

					EfficientAdapter adapter = new EfficientAdapter(FacebookFriendActivity.this, friendRecord);
					listFbFriend.setAdapter(adapter);
				}				
			});		

		}
	}



	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (pendingAction != PendingAction.NONE &&
				(exception instanceof FacebookOperationCanceledException ||
						exception instanceof FacebookAuthorizationException)) {
			
			pendingAction = PendingAction.NONE;
		} 
	}

	

	@Override
	public void onClick(View v) 
	{
		//CommonUtils.publishFeedDialog("", this, false);
		startActivity(new Intent(this, FacebookShare.class));
		
	}

}
