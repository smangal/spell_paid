package com.mathfriendzy.controller.multifriendzy.facebookfriends;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.multifriendzy.contacts.ContactBeans;
import com.mathfriendzy.utils.CommonUtils;
import com.spellfriendzypaid.R;

public class EfficientAdapter extends BaseAdapter 
{
	private ArrayList<ContactBeans> record 	= new ArrayList<ContactBeans>();
	
	Context context;
	String name;
	boolean[] sepAtIndex;
	Bitmap[] profileImageBitmap;
	//TextView titleTop;
	private LayoutInflater 	 inflater	= null;	
	boolean[] imageLoad;
	
	
	public EfficientAdapter(Context context, ArrayList<ContactBeans> record) 
	{
		inflater	 = LayoutInflater.from(context);
		this.record = record;
		this.context = context;
		addSeprator(record);			
		imageLoad = new boolean[record.size()];
		profileImageBitmap = new Bitmap[record.size()];
	}

	@Override
	public int getCount() 
	{
		return record.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int arg0) 
	{
		return 0;
	}

	@Override
	public View getView(final int position, View row, ViewGroup parent) 
	{		
		ViewHolder holder;
		if(row == null)
		{	
			row 					= inflater.inflate(R.layout.contact_list, null);
			holder 					= new ViewHolder();

			holder.txtName 			= (TextView) row.findViewById(R.id.txtContactName);
			holder.layout			= (LinearLayout) row.findViewById(R.id.titleLayout);
			holder.title			= (TextView) row.findViewById(R.id.title);
			holder.btnInvite		= (Button) row.findViewById(R.id.btnInvite);
			holder.imgFriend		= (ImageView) row.findViewById(R.id.imgContact);
			row.setTag(holder);	
		}//End if
		else 
		{
			holder = (ViewHolder) row.getTag();
		}// 
		
		if(imageLoad[position] == false)
		{		
			FacebookImageLoaderTask task = new FacebookImageLoaderTask(holder.imgFriend, position);
			task.execute();			
		}
		else
		{
			holder.imgFriend.setImageBitmap(profileImageBitmap[position]);
		}

		if(record.size() != 0)
		{
			name = record.get(position).getName();
			if ( sepAtIndex[position] )
			{					
				holder.populateView("  "+name.charAt(0), true);
			}
			else
			{
				holder.populateView(" ", false);
			}
			
		}//ENd if part of upcoming button	


		holder.btnInvite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				CommonUtils.publishFeedDialog(record.get(position).getId(), context, true);
			}
		});
		
		return row;
	}//End getView method



	/**
	 * uses to add seprator of months, week and tommorow
	 * @param items list of ContactDetail
	 */
	private void addSeprator(ArrayList<ContactBeans> items)
	{
		sepAtIndex = new boolean[items.size()];
		char ch = '0';

		for(int i = 0; i<items.size(); i++)
		{			
			if(items.get(i).getName().charAt(0) != ch)
			{
				sepAtIndex[i] = true;
			} else
			{
				sepAtIndex[i] = false;
			}

			ch = items.get(i).getName().charAt(0);
		}
	}


	private class ViewHolder
	{
		TextView txtName;
		TextView title;
		ImageView imgFriend;
		Button   btnInvite;
		LinearLayout layout;

		public void populateView(String value, boolean needSeparator) 
		{
			if (needSeparator) 		
			{
				title.setText(value);
				layout.setVisibility(View.VISIBLE);
			} else 
			{
				layout.setVisibility(View.GONE);
			}
			txtName.setText(""+name);
			
		}//END populateView method

	}//END viewholder class	


	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Bitmap>
	{
		private String strUrl = null;
		int index;
		ImageView img;
		Bitmap profileImage;

		public FacebookImageLoaderTask(ImageView imgFriend, int index)
		{
			this.index = index;	
			img = imgFriend; 
		}

		@Override
		protected Bitmap doInBackground(Void... params) 
		{			
			strUrl = FACEBOOK_HOST_NAME + record.get(index).getId()+ "/picture?type=small";	
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImage = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());				
			} 
			catch (MalformedURLException e) 
			{		
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return profileImage;
		}

		@Override
		protected void onPostExecute(Bitmap result) 
		{
			imageLoad[index] = true;
			img.setImageBitmap(result);
			profileImageBitmap[index] = result;
			super.onPostExecute(result);
		}
	}
	
	
}
