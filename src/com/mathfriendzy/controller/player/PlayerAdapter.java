package com.mathfriendzy.controller.player;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.utils.DialogGenerator;
import com.spellfriendzypaid.R;

/**
 * Player Adapter for the Player list view 
 * whcih set the data to the player Acitvity List View
 * @author Yashwant Singh
 *
 */
public class PlayerAdapter extends BaseAdapter
{
	private LayoutInflater mInflater 		   = null;
	private Context context 				   = null;
	private ArrayList<TempPlayer> playerList   = null;
	private int resource 					   = 0;
	private ViewHolder vholder 				   = null;
	
	public PlayerAdapter(Context context,  int resource , ArrayList<TempPlayer> playerList)
	{
		this.resource 	= resource;
		mInflater 		= LayoutInflater.from(context);
		this.context 	= context;
		this.playerList = playerList;
	}
	
	@Override
	public int getCount() 
	{
		return playerList.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int arg0) 
	{
		return 0;
	}

	@Override
	public View getView(final int index, View view, ViewGroup viewGroup) 
	{
		if(view == null)
		{
			vholder 			   = new ViewHolder();
			view 				   = mInflater.inflate(resource, null);
			vholder.txtPlayeName   = (TextView) view.findViewById(R.id.txtPlayerName);
			vholder.btnPlareEdit   = (Button)   view.findViewById(R.id.btnEditPlayer);
			vholder.btnPlayerResult= (Button)   view.findViewById(R.id.btnPlayerResult);
			view.setTag(vholder);			
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}
		vholder.txtPlayeName.setText(playerList.get(index).getFirstName() + " " + playerList.get(index).getLastName());
		
		vholder.btnPlareEdit.setOnClickListener(new OnClickListener() 
		{			
			@Override
			public void onClick(View v) 
			{		
				MathFriendzyHelper.showLoginRegistrationDialog(context);
				/*Intent intent = new Intent(context,EditPlayer.class);
				context.startActivity(intent);*/
			}
		});
		
		vholder.btnPlayerResult.setOnClickListener(new OnClickListener() 
		{			
			@Override
			public void onClick(View v) 
			{
				Translation transeletion = new Translation(context);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(context);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin"));
				transeletion.closeConnection();	
			}
		});
		
		return view;
	}

	
	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		TextView  txtPlayeName ;
		Button    btnPlareEdit;
		Button    btnPlayerResult;
	}
}
