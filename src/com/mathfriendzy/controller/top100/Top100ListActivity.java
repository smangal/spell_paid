package com.mathfriendzy.controller.top100;

import static com.mathfriendzy.utils.ITextIds.LBL_COUNTDOWN;
import static com.mathfriendzy.utils.ITextIds.LBL_POINTS;
import static com.mathfriendzy.utils.ITextIds.LBL_POS;
import static com.mathfriendzy.utils.ITextIds.LBL_SCHOOL_NAME;
import static com.mathfriendzy.utils.ITextIds.LBL_STUDENT_NAME;
import static com.mathfriendzy.utils.ITextIds.LBL_TEACHER_NAME;
import static com.mathfriendzy.utils.ITextIds.MF_SCHOOL;
import static com.mathfriendzy.utils.ITextIds.MF_STUDENT;
import static com.mathfriendzy.utils.ITextIds.MF_TEACHER;
import static com.mathfriendzy.utils.ITextIds.MF_TOP100;
import static com.mathfriendzy.utils.ITextIds.RATE_URL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.top100.JsonFileParser;
import com.mathfriendzy.model.top100.TopBeans;
import com.mathfriendzy.model.top100.TopListDatabase;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.spellfriendzy.facebookconnect.ShareActivity;
import com.spellfriendzypaid.R;

public class Top100ListActivity extends AdBaseActivity implements OnClickListener
{
	private TextView txtTitleScreen			= null;
	private TextView txtTop100				= null;
	private TextView txtCountDown			= null;
	private TextView txtPos					= null;
	private TextView txtTopName				= null;
	private TextView txtPoints				= null;
	private TextView txtTimer				= null;	
	private ListView top100List				= null;
	private Button btnShare					= null;
	private Button btnCloseShareTool		= null;
	private LinearLayout layoutShare		= null;

	private Button btnScreenShot			= null;
	private Button btnEmail					= null;
	private Button btnTwitter				= null;
	private Button btnFbShare				= null;

	private int id							= 0;
	private String jsonFile 				= null;
	private String topName					= null;
	private String subject					= null;
	private String body						= null;
	private String screenText				= null;
	private TopListDatabase db				= null;

	public static boolean TABLE_FLAG		= false;
	private ArrayList<TopBeans> topRecord	= null;
	private MyCountDownTimer countDownTimer = null;

	private long startTime					= 0;
	public static long finishTime			= 0;
	private final long interval 			= 1 * 1000;

	private JsonFileParser file				= null;
	private SharedPreferences	prefs		= null;
	private boolean flag;
	private DateTimeOperation date = null;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_top100_list);

		id = getIntent().getIntExtra("id", 0);
		getWidgetId();
		setWidgetText();

		topRecord 	= new ArrayList<TopBeans>();
		db			= new TopListDatabase();
		date		= new DateTimeOperation();

		getSharedPreferences();
		file = new JsonFileParser(this);	
		flag	= prefs.getBoolean("flag", false);

		if(finishTime/1000 <= 0)
		{
			flag = false;
		}
		

		jsonFile = db.fetchJsonFile(String.valueOf(id), getApplicationContext());

		try {
			startTime = getTimeStamp(jsonFile);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		if(!flag )
		{
			Log.e("", "It shows Flg is false");
			finishTime = startTime;	
		}
		topRecord = file.getTop100List(id, jsonFile);
		setTimer();
		Collections.sort(topRecord, new ListComparatorUsingSum());
		ListAdapter adapter = new ListAdapter(getApplicationContext(), id, topRecord);
		top100List.setAdapter(adapter);		

	}//END onCreate method	



	public void getSharedPreferences() 
	{
		prefs = this.getSharedPreferences("name", Context.MODE_PRIVATE);

		long date1 		= new Date().getTime();
		long date2 		= prefs.getLong("date",date1);		
		startTime		= prefs.getLong("start",0);	

		long diff = Math.abs(date1 - date2);	

		finishTime = startTime - diff;				
	}



	private long getTimeStamp(String jsonFile) throws JSONException 
	{
		long startTime;
		JSONObject jsonObj = new JSONObject(jsonFile);
		jsonObj = jsonObj.getJSONObject("data");

		startTime = Long.parseLong(jsonObj.getString("serverTimeStamp")) * 1000;
		return startTime;
	}


	private void setWidgetText()
	{
		String text;
		Translation translate = new Translation(this);
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier(LBL_COUNTDOWN);
		txtCountDown.setText(text);

		text  = translate.getTranselationTextByTextIdentifier(MF_TOP100);
		if(id == 1)
		{
			text  = text + " " + translate.getTranselationTextByTextIdentifier(MF_STUDENT);
			topName = translate.getTranselationTextByTextIdentifier(LBL_STUDENT_NAME);
		}
		if(id == 2)
		{
			text  = text + " " + translate.getTranselationTextByTextIdentifier(MF_TEACHER);
			topName = translate.getTranselationTextByTextIdentifier(LBL_TEACHER_NAME);
		}
		if(id == 3)
		{
			text  = text + " " + translate.getTranselationTextByTextIdentifier(MF_SCHOOL);
			topName = translate.getTranselationTextByTextIdentifier(LBL_SCHOOL_NAME);
		}
		txtTop100.setText(text);

		text = MathFriendzyHelper.getAppName(translate);
		txtTitleScreen.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_POS);
		txtPos.setText(text+": ");

		text = translate.getTranselationTextByTextIdentifier(LBL_POINTS);
		txtPoints.setText(text+": ");		

		txtTopName.setText(topName+":");

		screenText 	= translate.getTranselationTextByTextIdentifier("alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
		subject		= translate.getTranselationTextByTextIdentifier("infoEmailSubject");
		body		= translate.getTranselationTextByTextIdentifier("shareFBEmailMessage");

		translate.closeConnection();
	}//END setWidgetText method



	private void getWidgetId() 
	{
		txtCountDown		= (TextView) findViewById(R.id.txtCountDown);
		txtTimer			= (TextView) findViewById(R.id.txtTimer);
		txtTitleScreen		= (TextView) findViewById(R.id.txtTitleScreen);
		txtTop100			= (TextView) findViewById(R.id.txtTop100);
		txtPoints			= (TextView) findViewById(R.id.txtPoints);
		txtPos				= (TextView) findViewById(R.id.txtPos);
		txtTopName			= (TextView) findViewById(R.id.txtTopName);
		top100List			= (ListView) findViewById(R.id.top100List);
		btnShare			= (Button) findViewById(R.id.btnShare);
		btnCloseShareTool	= (Button) findViewById(R.id.btnCloseShareToolbar);
		layoutShare			= (LinearLayout) findViewById(R.id.layoutShare);

		btnEmail			= (Button) findViewById(R.id.btnMail);
		btnFbShare			= (Button) findViewById(R.id.btnFbSahre);
		btnScreenShot		= (Button) findViewById(R.id.btnScreenShot);
		btnTwitter			= (Button) findViewById(R.id.btnTwitterShare);

		btnEmail.setOnClickListener(this);
		btnFbShare.setOnClickListener(this);
		btnScreenShot.setOnClickListener(this);
		btnTwitter.setOnClickListener(this);

		btnCloseShareTool.setOnClickListener(this);
		btnShare.setOnClickListener(this);

	}//END getWidgetID method



	private void setTimer()
	{		
		countDownTimer = new MyCountDownTimer(finishTime, interval);		
		txtTimer.setText(String.valueOf(date.setTimeFormat(finishTime/1000)));		
		countDownTimer.start();
	}


	public class MyCountDownTimer extends CountDownTimer
	{
		public MyCountDownTimer(long startTime, long interval)
		{
			super(startTime, interval);
		}

		@Override
		public void onFinish() 
		{
			countDownTimer.cancel();
		}

		@Override
		public void onTick(long millisUntilFinished) 
		{
			finishTime = millisUntilFinished;
			txtTimer.setText(String.valueOf(date.setTimeFormat(finishTime/1000)));			
		}
	}


	private class ListComparatorUsingSum implements Comparator<TopBeans>
	{
		@Override
		public int compare(TopBeans arg0, TopBeans arg1) {
			return Integer.parseInt(arg1.getSum()) - Integer.parseInt(arg0.getSum());
		}

	}

	@Override
	protected void onResume() 
	{
		//getSharedPreferences();

		super.onResume();
	}

	@Override
	protected void onStop() 
	{
		countDownTimer.cancel();
		SharedPreferences.Editor editor = prefs.edit();
		if(id == 1)
		{
			editor.putBoolean("student_flag", true);
		}
		else if(id == 2)
		{
			editor.putBoolean("teacher_flag", true);
		}
		else if(id == 3)
		{
			editor.putBoolean("school_flag", true);
		}
		editor.putBoolean("flag", true);
		editor.putLong("start",finishTime);
		editor.putLong("date", new Date().getTime());
		editor.commit();
		super.onStop();
	}



	@Override
	public void onClick(View v) 
	{
		Translation transeletion = null;
		View view = getWindow().getDecorView().getRootView();
		view.setDrawingCacheEnabled(true);

		layoutShare.setVisibility(View.GONE);
		btnCloseShareTool.setVisibility(View.GONE);
		Bitmap b = view.getDrawingCache();
		Top100Activity.b = b;
		
		Intent intent;
		switch(v.getId())
		{		
		case R.id.btnShare:
			layoutShare.setVisibility(View.VISIBLE);
			btnCloseShareTool.setVisibility(View.VISIBLE);
			break;

		case R.id.btnCloseShareToolbar:
			break;

		case R.id.btnScreenShot:
			CommonUtils.saveBitmap(b, "DCIM/Camera", "screen");

			DialogGenerator generator = new DialogGenerator(this);
			transeletion = new Translation(this);
			transeletion.openConnection();
			generator.generateWarningDialog(screenText);
			transeletion.closeConnection();
			break;

		case R.id.btnMail:			
			//*******For Sharing ScreenShot ******************//
			String path = Images.Media.insertImage(getContentResolver(), b,"ScreenShot", null);
			Uri screenshotUri = Uri.parse(path);
			Intent emailIntent = new Intent(     android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body+ " "+RATE_URL);
			emailIntent.setType("image/png");
			try 
			{
				startActivity(Intent.createChooser(emailIntent, "Send email using"));
			} catch (android.content.ActivityNotFoundException ex) {
				Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
			}

			break;

		case R.id.btnFbSahre:		
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body+" "+RATE_URL);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("flag", false);
			intent.putExtra("id", R.id.btnFbSahre);
			startActivity(intent);
			break;

		case R.id.btnTwitterShare:			
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body);
			intent.putExtra("flag", false);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("id", R.id.btnTwitterShare);
			startActivity(intent);
			break;
		}

	}
}
