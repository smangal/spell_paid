package com.mathfriendzy.serveroperation;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import com.mathfriendzy.utils.CommonUtils;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


/**
 * This is the common asynck task for server operation
 * @author Yashwant Singh
 *
 */
public class MyAsyckTask  extends AsyncTask<Void, Void, HttpResponseBase>{

	private ArrayList<NameValuePair> nameValuePairs = null;
	private String url = null;
	private int requestCode;
	private Context context;
	private int diaologCode;
	private boolean isDisplayDialog;

	private HttpResponseInterface responseIntefrface;
	private String dialogMsg;

	private ProgressDialog pd;

	/**This is the common AsyncTask which will send a request to the server 
	 * and return the response in the form of HttpResponseBase class object
	 * @param nameValuePairs - if you are sending post request then put the nameValuePair value with its
	 * key and values and null for the url
	 * @param url - if you are sending a get request to the server then send a string url and put null for nameValuePairs
	 * @param requestCode - This is the request which you are sending , You will get the same request code in
	 *  the serverResponse(HttpResponseBase httpResponseBase,int requestCode) method
	 * @param context - This is current activity object
	 * @param responseIntefrface - Send the response interface object , Which you have impleted
	 * @param diaologCode - Send the dialog code
	 * @param isDisplayDialog - If you want to display the progress of loading the send it true otherwise false 
	 * @param dialogMsg - Send the message which you want to see in the progress dailog at the time of loading
	 */
	public MyAsyckTask(ArrayList<NameValuePair> nameValuePairs , String url , 
			int requestCode , Context context , 
			HttpResponseInterface responseIntefrface
			, int diaologCode , boolean isDisplayDialog
			, String dialogMsg){

		this.nameValuePairs = nameValuePairs;
		this.url = url;
		this.requestCode = requestCode;
		this.context = context;
		this.responseIntefrface = responseIntefrface;
		this.diaologCode = diaologCode;
		this.isDisplayDialog = isDisplayDialog;
		this.dialogMsg = dialogMsg;
	}

	@Override
	protected void onPreExecute() {

		if(isDisplayDialog){
			pd = ServerDialogs.getProgressDialog(context, dialogMsg , diaologCode);
			pd.show();
		}
		super.onPreExecute();
	}

	@Override
	protected HttpResponseBase doInBackground(Void... params) {
		String response = "";
		if(nameValuePairs != null)
			response = ServerOperation.readFromURL(nameValuePairs , 
					ServerOperation.getUrl(requestCode));
		else
			response = ServerOperation.readFromURL(url);
		
		if(response != null){
			HttpResponseBase httpResponseBase = new FileParser()
				.ParseJsonString(response, requestCode);
			return httpResponseBase;
		}
		return null;
	}

	@Override
	protected void onPostExecute(HttpResponseBase httpResponseBase) {

		if(isDisplayDialog){
			pd.cancel();
		}

		if(httpResponseBase != null){
			responseIntefrface.serverResponse(httpResponseBase, requestCode);
		}else{
			if(isDisplayDialog)
				CommonUtils.showInternetDialog(context);
		}
		super.onPostExecute(httpResponseBase);
	}
	
	
}
