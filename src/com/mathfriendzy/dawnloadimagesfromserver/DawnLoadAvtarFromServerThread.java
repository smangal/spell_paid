package com.mathfriendzy.dawnloadimagesfromserver;

import static com.mathfriendzy.utils.ICommonUtils.IMG_AVTAR_URL;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import android.content.Context;
import android.graphics.Bitmap;

import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.chooseAvtar.AvtarDTO;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.utils.CommonUtils;

public class DawnLoadAvtarFromServerThread implements Runnable{

	private String strUrl 				= null;
	ArrayList<AvtarDTO> avtarDataList 	= null;
	ArrayList<Bitmap>  bitmapList 		= null;
	ChooseAvtarOpration chooseAvtarObj  = null;
	Context context = null;
	
	public DawnLoadAvtarFromServerThread(ArrayList<AvtarDTO> avtarDataList , Context context){
		this.avtarDataList = avtarDataList;
		this.context       = context;
		
		bitmapList = new ArrayList<Bitmap>();
		
		chooseAvtarObj = new ChooseAvtarOpration();
		chooseAvtarObj.openConn(context);
		if(chooseAvtarObj.isAvtarTableExist())
		{
			//chooseAvtarObj.deleteFromAvtar();
		}
		else
		{
			chooseAvtarObj.createAvtarTable();
		}
		chooseAvtarObj.closeConn();
	}
	
	@Override
	public void run() {
		AvtarDTO avtarObj = null;
		try 
		{
			for( int i = 0 ; i < avtarDataList.size() ; i++)
			{				
				chooseAvtarObj.openConn(context);
				if(!chooseAvtarObj.isAvtarAlreadyExist(avtarDataList.get(i).getName()))
				{	avtarObj = new AvtarDTO();
					strUrl = IMG_AVTAR_URL + avtarDataList.get(i).getName();
					//avtarDataList.get(i).setImage(CommonUtils.getByteFromBitmap(getBitmap(strUrl)));
					avtarObj.setId(avtarDataList.get(i).getId());
					avtarObj.setName(avtarDataList.get(i).getName());
					avtarObj.setPrice(avtarDataList.get(i).getPrice());
					
					//Log.e(TAG, "inside do in background image Url " + strUrl);
					avtarObj.setImage(CommonUtils.getByteFromBitmap(CommonUtils.getBitmap(strUrl + ".png")));
					chooseAvtarObj.insertAvtar(avtarObj);
				}
				chooseAvtarObj.closeConn();
			}
			MainActivity.isAvtarLoaded = true;
		}
		catch (MalformedURLException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}		
	}

}
